import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
// import { Provider } from 'react-redux';
import App from './App';
// import { store } from './store';
import { ContextProvider } from './context/AppContext.js';


const root = ReactDOM.createRoot(document.getElementById('root'));


root.render(

  // <React.StrictMode>
  <ContextProvider>
    {/* <Provider store={store}> */}
    <BrowserRouter>
      <App />
    </BrowserRouter>
    {/* </Provider> */}
  </ContextProvider>
  // </React.StrictMode>
);



