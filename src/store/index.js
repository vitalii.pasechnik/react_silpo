import { combineReducers, legacy_createStore as createStore } from "redux";
import { sortReducer } from "./sortReducer";
import { appReducer } from "./appReducer";


const rootReducer = combineReducers({
    sortReducer,
    appReducer,
})

export const store = createStore(rootReducer)

