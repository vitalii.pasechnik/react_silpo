import { createContext, useContext, useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import API from '../assets/productsAPI';
import sortProductsList from "../utilities/sortProductsList";

const AppContext = createContext();

export const useAppContext = () => {
    return useContext(AppContext)
}


export const ContextProvider = ({ children }) => {

    const [productsAPI, setProductsAPI] = useState(API);
    const [sortedProductsAPI, setSortedProductsAPI] = useState([]);
    const [selectedSortEl, setSelectedSortEl] = useState('Оберіть сортування')
    const [allProdMenuSelectedItem, setAllProdMenuSelectedItem] = useState('');
    const [isVisibleAllProductsMenu, setIsVisibleAllProductsMenu] = useState(false);
    const [isHeaderBottomVisible, setIsHeaderBottomVisible] = useState(true);
    const [searchInputValue, setSearchInputValue] = useState('5');


    useEffect(() => {
        setSortedProductsAPI(JSON.parse(JSON.stringify(API)).sort((obj1, obj2) => obj1.available > obj2.available ? -1 : obj1.available < obj2.available ? 1 : 0));
        // console.log(setProductsAPI);
        console.log('test10');
    }, [productsAPI])

    return (
        <AppContext.Provider
            value={{
                sortProductsList,
                selectedSortEl, setSelectedSortEl,
                productsAPI, setProductsAPI,
                sortedProductsAPI, setSortedProductsAPI,
                allProdMenuSelectedItem, setAllProdMenuSelectedItem,
                isVisibleAllProductsMenu, setIsVisibleAllProductsMenu,
                isHeaderBottomVisible, setIsHeaderBottomVisible,
                API,
                searchInputValue, setSearchInputValue
            }}
        >
            {children}
        </AppContext.Provider>
    )
}