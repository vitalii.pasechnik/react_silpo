import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAppContext } from "../context/AppContext";
import sortProductsList from "../utilities/sortProductsList";
import useSideFilter from "./useSideFilter";

export default function useDropMenu(categoryList) {

    const [dropMenuList, setDropMemuList] = useState(categoryList)
    const { sortedProductsAPI, isVisibleAllProductsMenu, productsAPI, setSortedProductsAPI, selectedSortEl, setAllProdMenuSelectedItem } = useAppContext();
    // const { dropMenuSideFilter } = useSideFilter();
    // const navigate = useNavigate();

    const scrollCategories = (selectedEl, id) => {
        setDropMemuList(prevState => prevState.map(obj => obj.name === selectedEl && obj.id === id ? { ...obj, isSelected: true } : { ...obj, isSelected: false }))
    }

    const selectCategory = (e) => {
        e.preventDefault()
        let targetElName = e.target.closest('li').id
        setAllProdMenuSelectedItem(prev => prev = targetElName)
        let filteredProdList = productsAPI.filter(obj => Object.values(obj).includes(targetElName))
        setSortedProductsAPI(sortProductsList(filteredProdList, selectedSortEl))
        // console.log("id =", targetElName);
        // navigate(`/category/${path}`;
        console.log("targetElName =", targetElName);
        // dropMenuSideFilter(targetElName)
    }

    useEffect(() => {
        setDropMemuList(prevState => prevState.map(obj => obj = { ...obj, isSelected: false }))
    }, [isVisibleAllProductsMenu, sortedProductsAPI])

    return {
        dropMenuList, scrollCategories, selectCategory
    }
}