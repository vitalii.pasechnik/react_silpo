import { useEffect, useMemo, useRef, useState } from "react";
import { useAppContext } from "../context/AppContext"

export default function useSideFilter() {

    const { sortedProductsAPI, allProdMenuSelectedItem } = useAppContext();
    

    let sideFilter = useMemo(() => sortedProductsAPI.reduce((result, obj) => {
        for (const [key, value] of Object.entries(obj)) {
            if (value === allProdMenuSelectedItem && key === 'category') {
                result[obj.menu_level_2] ? result[obj.menu_level_2]++ : result[obj.menu_level_2] = 1
            } else if (value === allProdMenuSelectedItem && key === 'menu_level_2' && obj.hasOwnProperty('menu_level_3')) {
                result[obj.menu_level_3] ? result[obj.menu_level_3]++ : result[obj.menu_level_3] = 1
            }
        }
        console.log('test_D');
        return result
    }, {}), [allProdMenuSelectedItem])

  
    // sideFilter.current = useEffect(() => sortedProductsAPI.reduce((result, obj) => {
    //     for (const [key, value] of Object.entries(obj)) {
    //         if (key === 'title' && value.toLowerCase().includes(searchInputValue)) {
    //             result[obj.category] ? result[obj.category]++ : result[obj.category] = 1
    //         }
    //     }
    //     console.log('test_F');
    //     return result
    // }, {}), [searchInputValue])

    // console.log(sideFilter.current);

  
    const categoriesForFilter = useMemo(() => Object.entries(sideFilter).sort((a, b) => {
        console.log('test1');
        return b[1] - a[1];
    }), [sideFilter]);


    return {
        categoriesForFilter
    }

}