import { useEffect, useState } from "react";
import { useAppContext } from "../context/AppContext";
import sortProductsList from "../utilities/sortProductsList";

export default function useSearch() {

    const { productsAPI, setSortedProductsAPI, selectedSortEl, setAllProdMenuSelectedItem } = useAppContext();

    const searchProduct = (query) => {
        let filteredProdList = productsAPI.filter(obj => obj.title.toLowerCase().includes(query))
        setSortedProductsAPI(sortProductsList(filteredProdList, selectedSortEl))
        setAllProdMenuSelectedItem('')
    }

    return {
        searchProduct
    }
}