import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import { useAppContext } from './context/AppContext';


import { Layout, ContentWrapper, Cart, LoaderMain, HeaderSearch } from './imports/components'

function App() {

    const { searchInputValue } = useAppContext()

    // const dispath = useDispatch()

    console.log(searchInputValue);

    return (


        <Routes>
            <Route path="/" element={<Layout />}>
                <Route path="/" element={<Navigate to="/silpo/products" replace />} />

                <Route path="/:page/:param" element={<ContentWrapper />} />

                <Route path="cart" element={<Cart />} />
                <Route path="*" element={<LoaderMain />} />
            </Route>
        </Routes>


    )
}

export default App;
