import React from "react";
import classes from './Cart.module.scss';

import { ReactComponent as BasketEmptySVG } from '../../../images/basket_empty_icon.svg';
import { ReactComponent as MastercardSVG } from '../../../images/mastercard_icon.svg';
import { useEffect } from "react";

const Cart = () => {

    return (
        <div className={classes.cart_page}>
            <div className={classes.cart_page_content}>
                <div className={classes.cart_page_list}>
                    <div className={classes.cart_page_header}>
                        <h1 className={classes.cart_title}>
                            <span>Кошик</span>
                        </h1>
                    </div>
                    <div className={classes.mastercard_notification}>
                        <div>
                            <i className={`${classes.icon} ${classes.icon_mastercard} ${classes.raw}`}>
                                <MastercardSVG />
                            </i>
                        </div>
                        <span>Оплатіть це замовлення онлайн карткою Mastercard® – і персональна пропозиція на доставку за 1 грн ваша! Застосуємо її автоматично на наступне замовлення з «Сільпо».</span>
                    </div>
                    <div className={`${classes.no_data} ${classes.cart_item_list}`}>
                        <div className={classes.no_data_content}>
                            <i className={`${classes.icon} ${classes.icon_cart} ${classes.raw}`}>
                                <BasketEmptySVG />
                            </i>
                            <div className={classes.empty_cart_text}>
                                <span>У вашому кошику немає товарів</span>
                            </div>
                            <a href="/">
                                <button type="button" className={`${classes.btn} ${classes.btn_primary}`}>
                                    <span>Обрати товари</span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Cart;