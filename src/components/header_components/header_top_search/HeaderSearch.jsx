import React from "react";
import { useState, useRef } from "react";

import classes from './HeaderSearch.module.css';

import clearBtn_Icon from '../../../images/clear_input_button.svg'
import { ReactComponent as SearchSVG } from '../../../images/search_icon.svg'
import LoaderSearch from "../header_top_search_loader/LoaderSearch";
import { Link, Navigate, useNavigate, useSearchParams } from "react-router-dom";
import useSearch from "../../../hooks/useSearch";
import { useEffect } from "react";
import { useAppContext } from "../../../context/AppContext";

const HeaderSearch = () => {

    const { searchProduct } = useSearch();
    // const { setSearchInputValue } = useAppContext()
    const navigate = useNavigate();

    const [isInputFocused, setIsInputFocused] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [searchParams, setSearchParams] = useSearchParams();
    const searchQuery = searchParams.get('find') || '';

    const inputEl = useRef(null);
    const inputFocused = () => {
        setIsInputFocused(true);
        inputEl.current.focus();
    }
    const inputUnFocused = () => {
        setIsInputFocused(false);
        inputEl.current.blur();
    }

    const handlerChangeInput = (e) => setInputValue(prev => prev = e.target.value)
    const handlerClearInput = () => setInputValue(prev => prev = '')

    const handleSubmit = (e) => {
        if ((e.code === "Enter" || e.code === "NumpadEnter") && inputValue) {
            e.preventDefault();
            searchProduct(inputValue)
            navigate('/search/all', { replace: true })
            console.log(searchQuery);
        }
    }

    const addSearchParam = (e) => {
        if ((e.code === "Enter" || e.code === "NumpadEnter") && inputValue) {
            setSearchParams({ find: inputValue })
            inputUnFocused();
        }
    }

    useEffect(() => {
        document.body.addEventListener('keydown', (e) => e.code === "Escape" && inputUnFocused());
    }, [])

    return (
        <div className={`header_top ${classes.search_field}`}>
            <div
                className={`${classes.backLayer} ${isInputFocused && classes.visible}`}
                onMouseDown={inputUnFocused}
            >
            </div>
            <div
                className={`header_top ${classes.search} ${isInputFocused && classes.focused}`}
                onClick={inputFocused}
            >
                <SearchSVG className={classes.searchIcon} />
                <input
                    type="text" name="search"
                    placeholder='Пошук на сайті'
                    autoComplete="off"
                    value={inputValue}
                    ref={inputEl}
                    onMouseDown={inputFocused}
                    onChange={handlerChangeInput}
                    onKeyDown={handleSubmit}
                    onKeyUp={addSearchParam}
                />
                <div
                    className={`${classes.inputClearBtn} ${inputValue && isInputFocused && classes.visible}`}
                    onClick={handlerClearInput}
                >
                    <img alt='button' src={clearBtn_Icon} />
                </div>
                <div className={`${classes.dropdownMenu} ${isInputFocused && inputValue && classes.visible}`}>
                    <LoaderSearch />
                    {inputValue}
                </div>
            </div>
        </div>
    )
}

export default HeaderSearch;











