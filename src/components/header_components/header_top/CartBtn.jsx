import React from "react";
import { Link } from "react-router-dom";

import classes from './Header.module.scss';

import { ReactComponent as BasketSVG } from '../../../images/basket_icon.svg';

const CartBtn = ({ children }) => {

    return (
        <div className={classes.basket_wrapper} id="cart">
            <Link to='/cart' className={classes.basket_btn} tabIndex='0' title='Перейти в кошик' aria-label='Перейти в кошик' >
                <BasketSVG />
                <div style={{ margin: '0 auto' }}>
                    {children}
                </div>
                <del>&nbsp;</del>
            </Link>
        </div>
    )
}

export default CartBtn;