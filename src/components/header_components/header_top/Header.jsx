import { Fragment, React } from 'react';

import classes from './Header.module.scss';

import logo_icon from '../../../images/silpo_logo.svg';

import HeaderBottom from '../header_bottom/HeaderBottom';
import HeaderSearch from '../header_top_search/HeaderSearch';
import UserInfo from '../header_top_userInfo/UserInfo';
import AllProductsBtn from './AllProductsBtn';
import CartBtn from './CartBtn';
import { useAppContext } from '../../../context/AppContext';
import { Link } from 'react-router-dom';
import sortProductsList from '../../../utilities/sortProductsList';


const Header = () => {

    // const { isVisibleAllProductsMenu, isHeaderBottomVisible } = useAppContext();
    const { isVisibleAllProductsMenu, isHeaderBottomVisible, setAllProdMenuSelectedItem, setSelectedSortEl, setSortedProductsAPI, productsAPI } = useAppContext();
    const showAllCategories = () => {
        setAllProdMenuSelectedItem('');
        setSelectedSortEl('Оберіть сортування');
        setSortedProductsAPI(productsAPI)
        sortProductsList(productsAPI)
    }

    return (
        <Fragment>
            <div className={`
                ${classes.header} 
                ${isHeaderBottomVisible ? classes.header_disabled_shadow : ''} 
                ${isVisibleAllProductsMenu ? classes.scrollable_catalog : ''}`
            }
            >
                {isVisibleAllProductsMenu &&
                    <div className={classes.shadow_layer}></div>
                }
                <div className={classes.header_content_wrapper}>
                    <div className={classes.header_content}>
                        <div className={classes.header_top}>
                            <Link to="/silpo/products" onClick={showAllCategories}>
                                <div className={classes.header_logo} title='Онлайн замовлення товарів з «Сільпо»' >
                                    <img alt='Logo' src={logo_icon} />
                                </div>
                            </Link>
                            <AllProductsBtn>Всі товари</AllProductsBtn>
                            <HeaderSearch />
                            <UserInfo />
                            <CartBtn>Кошик</CartBtn>
                        </div>
                    </div>
                </div>
                <div className={`header__bottom__wrapper ${!isHeaderBottomVisible ? 'hide' : ''}`}>
                    <HeaderBottom />
                </div>
            </div>
        </Fragment >
    )
}

export default Header;