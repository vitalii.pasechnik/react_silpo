import React, { useEffect, useRef, useState } from "react";

import classes from './Header.module.scss';

import { ReactComponent as CategoriesSVG } from '../../../images/categories_icon.svg';
import { ReactComponent as ChevronDownSVG } from '../../../images/chevron_down_icon.svg';

import AllProductsDropMenu from '../header_top_drop_menu/AllProductsDropMenu';
import { useAppContext } from "../../../context/AppContext";

const AllProductsBtn = ({ children }) => {

    const { setIsVisibleAllProductsMenu } = useAppContext();

    const toggleAllProductsMenu = () => {
        document.body.scrollIntoView();
        setIsVisibleAllProductsMenu(prev => !prev);
    }

    const allProdDropMenuEl = useRef();
    const handlerHideAllProductsMenu = (e) => {
        const path = e.path || (e.composedPath && e.composedPath());
        if (path && !path.includes(allProdDropMenuEl.current) || e.code === "Escape")
            setIsVisibleAllProductsMenu(false);
    }

    useEffect(() => {
        document.body.addEventListener('click', handlerHideAllProductsMenu);
        document.body.addEventListener('keydown', handlerHideAllProductsMenu);
    }, [])

    return (
        <div
            className={classes.all_products_btn} role="button"
            onClick={toggleAllProductsMenu}
            ref={allProdDropMenuEl}
        >
            <CategoriesSVG />
            {children}
            <ChevronDownSVG />
            <AllProductsDropMenu />
        </div>
    )
}

export default AllProductsBtn;