import React from "react";

import classes from './LoaderSearch.module.css';

const LoaderSearch = () => (
    <div className={classes.loader} />
)

export default LoaderSearch;