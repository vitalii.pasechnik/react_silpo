import React from "react";

import allProductCategories from '../../../assets/allProductCategoies';
import classes from './AllProductsDropMenu.module.scss';

import useDropMenu from "../../../hooks/useDropMenu";
import { ReactComponent as ChevronRightSVG } from '../../../images/chevron_right_icon.svg';
import AllProductsDropMenu_Lv2 from "./AllProductsDropMenu_Lv2";
import { useAppContext } from "../../../context/AppContext";
import { Link, useParams } from "react-router-dom";

const AllProductsDropMenu = () => {

    const { dropMenuList, scrollCategories, selectCategory } = useDropMenu(allProductCategories)
    const { isVisibleAllProductsMenu } = useAppContext();

    return (
        isVisibleAllProductsMenu
        &&
        <div className={classes.menu_wrapper} >
            <div className={classes.main_menu}>
                <ul className={`${classes.main_menu_levels} ${classes.level_1}`} id='allProductMenu'>
                    {dropMenuList.map(el =>
                        <li
                            className={el.isSelected ? classes.active : ""}
                            role="link" tabIndex="0" key={el.id} id={el.name}
                            onMouseOver={() => scrollCategories(el.name, el.id)}
                            onClick={selectCategory}
                        >
                            {/* <Link to={`/category/${el.path}`} className={classes.link}></Link> */}
                            <Link to={`/category/${el.path}`} className={classes.link}>
                                <div>
                                    <img className={classes.menu_item_icon} src={el.img} alt="" title={el.name} aria-hidden="true" />
                                    {el.name}
                                    {el.isSelected
                                        ?
                                        <AllProductsDropMenu_Lv2
                                            list={el.menu_level_2}
                                            path={el.path}
                                            name={el.name}
                                            id={el.name}
                                        />
                                        :
                                        <i className={classes.icon_chevron_right}>
                                            <ChevronRightSVG />
                                        </i>
                                    }
                                </div>
                            </Link>
                        </li>
                    )}
                </ul>
            </div>
        </div>
    )
}

export default AllProductsDropMenu;


