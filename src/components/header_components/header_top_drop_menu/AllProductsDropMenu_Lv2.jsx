import React from "react";

import classes from './AllProductsDropMenu.module.scss';

import useDropMenu from "../../../hooks/useDropMenu";
import { ReactComponent as ChevronRightSVG } from '../../../images/chevron_right_icon.svg';
import AllProductsDropMenu_Lv3 from "./AllProductsDropMenu_Lv3";
import { Link, useParams } from "react-router-dom";

const AllProductsDropMenu_Lv2 = ({ list, path, name, id }) => {

    const { dropMenuList, scrollCategories, selectCategory } = useDropMenu(list)

    return (
        <div className={classes.main_menu_levels_wrapper} id='allProductMenu_lvl_2'>
            <ul className={`${classes.main_menu_levels} ${classes.level_2}`}>
                <li className={classes.capital} id={id} onClick={selectCategory}>
                    <Link to={`/category/${path}`} className={classes.link}>
                        <div>
                            <span>Все у&nbsp;</span>
                            {name}
                        </div>
                    </Link>
                </li>
                {dropMenuList.map(el =>
                    el.hasOwnProperty('menu_level_3')
                        ?
                        <li
                            className={el.isSelected ? classes.active : ""}
                            role="link" tabIndex="0" key={el.id} id={el.name}
                            onMouseOver={() => scrollCategories(el.name, el.id)}
                            onClick={selectCategory}
                        >
                            <Link to={`/category/${el.path}`} className={classes.link}>
                                <div>
                                    {el.name}
                                    {el.isSelected
                                        ?
                                        <AllProductsDropMenu_Lv3
                                            list={el.menu_level_3}
                                            path={el.path}
                                            name={el.name}
                                            id={el.name}
                                        />
                                        :
                                        <i className={classes.icon_chevron_right}>
                                            <ChevronRightSVG />
                                        </i>
                                    }
                                </div>
                            </Link>
                        </li>
                        :
                        <li
                            className={el.isSelected ? classes.active : ""}
                            role="link" tabIndex="0" key={el.id} id={el.name}
                            onMouseOver={() => scrollCategories(el.name, el.id)}
                            onClick={selectCategory}
                        >
                            <Link to={`/category/${el.path}`} className={classes.link}>
                                <div>
                                    {el.name}
                                </div>
                            </Link>
                        </li>
                )}
            </ul>
        </div>
    )
}

export default AllProductsDropMenu_Lv2;