import React from "react";
import { Link, useParams } from "react-router-dom";
import useDropMenu from "../../../hooks/useDropMenu";

import classes from './AllProductsDropMenu.module.scss';

const AllProductsDropMenu_Lv3 = ({ list, path, name, id }) => {

    const { dropMenuList, scrollCategories, selectCategory } = useDropMenu(list)

    return (
        <ul className={`${classes.main_menu_levels} ${classes.level_3} ${classes.visible}`}>
            <li className={classes.capital} id={id} onClick={selectCategory}>
                <Link to={`/category/${path}`} className={classes.link}>
                    <div>
                        <span>Все у&nbsp;</span>
                        {name}
                    </div>
                </Link>
            </li>
            {dropMenuList.map(el =>
                <li
                    className={el.isSelected ? classes.active : ""}
                    role="link" tabIndex="0" key={el.id} id={el.name}
                    onMouseOver={() => scrollCategories(el.name, el.id)}
                    onClick={selectCategory}
                >
                    <Link to={`/category/${el.path}`} className={classes.link}>
                        <div>
                            {el.name}
                        </div>
                    </Link>
                </li>
            )}
        </ul>
    )
}

export default AllProductsDropMenu_Lv3;