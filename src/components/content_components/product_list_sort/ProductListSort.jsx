import React, { useEffect, useState, useRef } from "react";

import classes from './ProductListSort.module.scss';

import { useAppContext } from "../../../context/AppContext";
import { ReactComponent as SortMenuSVG } from '../../../images/sort_menu_icon.svg';


const ProductListSort = ({ widgetElements, ...props }) => {

    const { sortedProductsAPI, setSortedProductsAPI, sortProductsList, selectedSortEl, setSelectedSortEl } = useAppContext();
    const [sortMenuClassActive, setSortMenuClassActive] = useState(false)

    const toggleSortMenu = () => setSortMenuClassActive(prev => !prev)

    const handlerSortSelect = (widgetName) => {
        widgetElements.map(el => {
            if (el === widgetName) {
                setSelectedSortEl(prev => prev = widgetName);
                setSortedProductsAPI(sortProductsList(sortedProductsAPI, widgetName));
            }
        })
        setSortMenuClassActive(false);
    }

    const sortMenuEl = useRef(null);
    const handleOutsideClick = (e) => {
        const path = e.path || (e.composedPath && e.composedPath());
        if (path && !path.includes(sortMenuEl.current)) setSortMenuClassActive(false);
        // if (!e.path.includes(sortMenuEl.current)) setSortMenuClassActive(false);
        // console.log(e);
    }

    useEffect(() => {
        document.body.addEventListener('mousedown', handleOutsideClick);
    }, [])

    return (
        <div className={classes.sort_menu} id="sort_widget" ref={sortMenuEl}>
            <div className={`${classes.sort_widget_wrapper} ${sortMenuClassActive ? classes.active : ''}`} >
                <div className={classes.sort_widget_title} onClick={toggleSortMenu} >
                    <span>{selectedSortEl}</span>
                    <i className={classes.icon}>
                        <SortMenuSVG />
                    </i>
                </div>
                <ul className={classes.sort_widget_list} >
                    {widgetElements.map((widgetName, ind) =>
                        <li
                            key={`${widgetName}_${ind}`}
                            className={selectedSortEl === widgetName ? classes.sort_widget_selected : ""}
                            onClick={() => handlerSortSelect(widgetName)}
                        >
                            {widgetName}
                        </li>
                    )}
                </ul>
            </div>
        </div>
    )
}

export default ProductListSort;


