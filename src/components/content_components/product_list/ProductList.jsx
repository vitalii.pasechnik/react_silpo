import React from "react";

import classes from './ProductList.module.scss';

import { useAppContext } from "../../../context/AppContext";
import ProductListItem from "../product_list_item/ProductListItem";


const ProductList = ({ ...props }) => {

    const { sortedProductsAPI, setSortedProductsAPI } = useAppContext()

    return (
        <ul className={classes.product_list}>
            {sortedProductsAPI.map((product) =>
                <ProductListItem {...product} key={product.id} />
            )}
        </ul>
    )
}

export default ProductList;