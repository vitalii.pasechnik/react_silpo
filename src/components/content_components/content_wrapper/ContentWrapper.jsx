import React, { useContext } from "react";

import classes from './ContentWrapper.module.scss';

import { ProductList, ProductListSort, ProductListFilter, PromoBanner } from "../../../imports/components"
import { useAppContext } from "../../../context/AppContext";


const ContentWrapper = () => {

    const { allProdMenuSelectedItem } = useAppContext()

    return (
        <div className={classes.container} style={{ marginTop: '150px' }}>
            <div className={classes.content}>
                {/* <PromoBanner /> */}
                <div className={classes.page}>
                    <h1 className={classes.category_page_heading}>
                        {allProdMenuSelectedItem ? allProdMenuSelectedItem : `«Сільпо»`}
                    </h1>
                    <div className={classes.category_page_header}>
                        <ProductListSort
                            widgetElements={['Спочатку популярні', 'За рейтингом', 'Спочатку акційні', 'Спочатку дешевші', 'Спочатку дорожчі', 'Від А до Я', 'Від Я до А']}
                        />
                    </div>
                    <div className={classes.category_page_content}>
                        {allProdMenuSelectedItem
                            &&
                            <div
                                className={`${classes.category_filter_wrapper} ${classes.category_filter_column}`}
                                style={{ alignSelf: 'flex-start' }}
                            >
                                <ProductListFilter />
                            </div>
                        }
                        <div className={classes.product_list_wrapper}>
                            <ProductList />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContentWrapper;