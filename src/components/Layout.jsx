import { Outlet } from "react-router-dom";
import { Header } from '../imports/components'

const Layout = () => {
    return (
        <div className='App' >
            <div className="layout" >
                <Header />

                <Outlet /> 

            </div>
        </div>
    )
}

export default Layout;